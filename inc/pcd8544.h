/**
 * @file		pcd8544.h
 * @author		Chugay E.A.
 * @version		0.02
 * @date		11.04.2021
 *
 * @brief       Функции, обеспечивающие инициализацию перефирийных модулей МК,
 *              инициализацию контроллера PCD8544 и их совместную работу.
 * @details
 * File used UTF-8 code page. Don't convert other codepage for correct work.
 */
 
#ifndef PCD8544_H_
#define PCD8544_H_

/*------------------------------------------------------------------ Includes */
#include "display_config.h"
#include "frame_builder.h"

/*------------------------------------------------------- Function prototypes */
/**
 * @brief	Инициализация SPI, используемых GPIO, передача команд инициализации в дисплей.
 * @param	Нет
 * @retval	Нет
 */
void init_display (void);

/**
 * @brief	Передача команды отключения в дисплей, де-инициализация SPI и используемых GPIO.
 * @param	Нет
 * @retval	Нет
 */
void deinit_display (void);

/**
 * @brief	Настройка контрастности дисплея
 * @param	value - новое значение контрастности
 * @retval	Нет
 */
void display_set_contrast (uint8_t value);

/**
 * @brief	Функция передачи кадра в дисплей.
 * @param	signal - переменная для управления построением кадра
 * @param	void(*frame)(uint8_t) - указатель на функцию построения изображения (отрисовки кадра).
 * @retval	Нет.
 */
void send_frame_to_display (uint8_t signal, void(*frame)(uint8_t));
	
#endif //	PCD8544_H_
