/**
 * @file		sh1106.с
 * @author		Chugay E.A.
 * @version		0.02
 * @date		26.05.2019
 *
 * @brief       Функции, обеспечивающие инициализацию перефирийных модулей МК,
 *              инициализацию контроллера SH1106 и их совместную работу.
 * @details
 * File used UTF-8 code page. Don't convert other codepage for correct work.
 */

/*------------------------------------------------------------------ Includes */
#include "sh1106.h"

/*-------------------------------------------------------- External variables */
extern volatile uint8_t frame_buf[BUF_SIZE];

/*--------------------------------------------------------- Private variables */
static union {
    struct {
        volatile uint8_t frame_buf_busy	 :1;	//  Флаг равен 1 при передаче кадра в дисплей
        volatile uint8_t frame_buf_ready :1;	//  Флаг должен быть установлен в 1 после окончания формирования кадра
        volatile uint8_t page_switch	 :6;	//  номер передаваемой страницы буфера
    };
    volatile uint8_t value;
} display_flag;

/*----------------------------------------------- Private function prototypes */
/**
 * @brief	Инициализация SPI для работы с SH1106
 * @param	Нет
 * @retval	Нет
 */
void sh1106_SPI_init (void);

/**
 * @brief	Отключение SPI, DMA, и перевод выводов в плавающий вход
 * @param	Нет
 * @retval	Нет
 */
void sh1106_SPI_deinit (void);

/**
 * @brief	Стартовая инициализация контроллера дисплея
 * @param	Нет
 * @retval	Нет
 */
void sh1106_start_init (void);

/**
 * @brief	Настройка яркости дисплея (0..255)
 * @param	value - новое значение контрастности
 * @retval	Нет
 */
void sh1106_set_contrast (uint8_t value);

/**
 * @brief	Запуск передачи буфера в дисплей
 * @param	Нет
 * @retval	Нет
 */
void sh1106_DMA_send (void);

/**
 * @brief	Передача команды отключения дисплея
 * @param	Нет
 * @retval	Нет
 */
void sh1106_standby (void);

/**
 * @brief	Отправка одного байта в дисплей
 * @param	data - передаваемый байт
 * @retval	Нет
 */
void sh1106_send_byte (uint8_t data);



/*----------------------------------------------------------------------------*/
/*---------------------------------------------------------------- Functions: */
/*------------------------------------------------------------------ Функции: */
/*----------------------------------------------------------------------------*/

//  Инициализация дисплея
void init_display (void)
{
    sh1106_SPI_init();
    sh1106_start_init();
}

//  Де-инициализация дисплея
void deinit_display (void)
{
    sh1106_standby();
    sh1106_SPI_deinit();
}

//  Настройка контрастности дисплея
void display_set_contrast (uint8_t value)
{
    sh1106_set_contrast(value);
}

//  передача кадра в дисплей
void send_frame_to_display (uint8_t signal, void(*frame)(uint8_t))
{
    if (!display_flag.frame_buf_busy) {
        frame_clear_buf();
        (*frame)(signal);
        display_flag.frame_buf_ready = 1;
    }
    sh1106_DMA_send();
}



/*----------------------------------------------------------------------------*/
/*--------------------------------------------------------- Private function: */
/*------------------------------------------------------- Внутренние функции: */
/*----------------------------------------------------------------------------*/

#ifdef STM32F0_SERIES
#ifdef DISPLAY_SPI1
void sh1106_SPI_init(void)
{
    RCC->AHBENR |= (RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN);
    //	PB1 - RESET
    GPIOB->MODER   &= ~GPIO_MODER_MODER1;		//	input
    GPIOB->OTYPER  &= ~GPIO_OTYPER_OT_1;		//	push/pull
    GPIOB->PUPDR   &= ~GPIO_PUPDR_PUPDR1;		//	no pull-up or pull-down
    GPIOB->MODER   |= GPIO_MODER_MODER1_0;		//	output
    GPIOB->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR1;	//	high speed
    //	PA6 - CHIP ENABLE
    GPIOA->MODER   &= ~GPIO_MODER_MODER6;		//	input
    GPIOA->OTYPER  &= ~GPIO_OTYPER_OT_6;		//	push/pull
    GPIOA->PUPDR   &= ~GPIO_PUPDR_PUPDR6;		//	no pull-up or pull-down
    GPIOA->MODER   |= GPIO_MODER_MODER6_0;		//	output
    GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR6;	//	high speed
    //	PA4 - SEND_DATA/SEND_CMD
    GPIOA->MODER   &= ~GPIO_MODER_MODER4;		//	input
    GPIOA->OTYPER  &= ~GPIO_OTYPER_OT_4;		//	push/pull
    GPIOA->PUPDR   &= ~GPIO_PUPDR_PUPDR4;		//	no pull-up or pull-down
    GPIOA->MODER   |= GPIO_MODER_MODER4_0;		//	output
    GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR4;	//	high speed
    //	PA7 - MOSI
    GPIOA->MODER  &= ~GPIO_MODER_MODER7;		//	input
    GPIOA->OTYPER &= ~GPIO_OTYPER_OT_7;			//	push/pull
    GPIOA->PUPDR  &= ~GPIO_PUPDR_PUPDR7;		//	no pull-up or pull-down
    GPIOA->MODER  |= GPIO_MODER_MODER7_1;		//	alternate func.
    GPIOA->AFR[0] &= ~GPIO_AFRL_AFSEL7;			//	SPI1 MOSI
    //	PA5 - SCK
    GPIOA->MODER  &= ~GPIO_MODER_MODER5;		//	input
    GPIOA->OTYPER &= ~GPIO_OTYPER_OT_5;			//	push/pull
    GPIOA->PUPDR  &= ~GPIO_PUPDR_PUPDR5;		//	no pull-up or pull-down
    GPIOA->MODER  |= GPIO_MODER_MODER5_1;		//	alternate func.
    GPIOA->AFR[0] &= ~GPIO_AFRL_AFSEL5;			//	SPI1 SCK
    //	CPOL=0, CPHA=0, 8 bit frame, 3.0 MHz CLK:
    RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;			//	подать тактирование на модуль SPI2
    SPI1->CR1 = 0x0000;
    SPI1->CR2 = 0x0000;
    SPI1->CR1 |= (SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE | SPI_CR1_MSTR |
                  SPI_CR1_BR_1 | SPI_CR1_BR_0 | SPI_CR1_SSI | SPI_CR1_SSM);	//	2-wire state
    SPI1->CR1 |= SPI_CR1_SPE;					//	включить SPI
#ifdef DMA_MODE
    //	настройка источника и приемника:
    RCC->AHBENR |= RCC_AHBENR_DMAEN;				//	тактирование DMA
    DMA1_Channel3->CPAR = (uint32_t)&SPI_DR;		//	адрес регистра перефирии
    DMA1_Channel3->CMAR = (uint32_t)&frame_buf[0];	//	массив данных в памяти
    DMA1_Channel3->CNDTR = SCREEN_W;				//	количество байт для передачи (одна строка)
    //	инкрементация для перехода по элементам массива в памяти
    //	чтение из памяти, прерывание при передаче половины и окончании
    DMA1_Channel3->CCR = 0;
    DMA1_Channel3->CCR |= (DMA_CCR_MINC | DMA_CCR_DIR | DMA_CCR_TCIE);
    SPI1->CR2 |= SPI_CR2_TXDMAEN;					//	разрешить работу SPI2 с DMA
//	NVIC_SetPriority(DMA1_Channel2_3_IRQn, X);		//	приоритет прерывания
    NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);			//	разрешить прерывания от DMA, канал 3
#endif	//	DMA MODE
}

//	отключение перефирии, обеспечивающей работу дисплея
void sh1106_SPI_deinit(void)
{
    NVIC_DisableIRQ (SPI1_IRQn);
    RCC->APB2ENR &= ~RCC_APB2ENR_SPI1EN;
#ifdef DMA_MODE
    NVIC_DisableIRQ (DMA1_Channel2_3_IRQn);
    RCC->AHBENR &= ~RCC_AHBENR_DMAEN;
#endif	//	DMA MODE
    GPIOB->MODER &= ~GPIO_MODER_MODER1;		//	input	PB1 - RESET
    GPIOA->MODER &= ~GPIO_MODER_MODER6;		//	input	PA6 - CHIP ENABLE
    GPIOA->MODER &= ~GPIO_MODER_MODER4;		//	input	PA4 - SEND_DATA/SEND_CMD
    GPIOA->MODER &= ~GPIO_MODER_MODER7;		//	input	PA7 - MOSI
    GPIOA->MODER &= ~GPIO_MODER_MODER5;		//	input	PA5 - SCK
}
#endif	//	DISPLAY_SPI1
#endif	//	STM32F0_SERIES

#ifdef DMA_MODE
#ifdef STM32F0_SERIES
#ifdef DISPLAY_SPI1
//	Запуск пересылки массива данных в дисплей
void sh1106_DMA_send(void)
{
    // передаем данные если модуль DMA был отключен и установлен флаг готовности буфера кадра:
    if (!(DMA1_Channel3->CCR & DMA_CCR_EN) && display_flag.frame_buf_ready) {
        SEND_CMD;												//	режим передачи команд
        TX_ENABLE;												//	разрешаем обмен
        sh1106_send_byte(0xB0 | display_flag.page_switch);		//	адрес страницы
        sh1106_send_byte(0x10);									//	смещение
        sh1106_send_byte(0x02);									//	на 2 колонки
        while (SPI_SR & SPI_SR_BSY);							//	ожидаем окончания передчи команды
        SEND_DATA;												//	режим передачи данных
        DMA1_Channel3->CMAR = (uint32_t)&frame_buf[SCREEN_W * display_flag.page_switch];
        DMA1_Channel3->CNDTR = SCREEN_W;						//	количество байт для передачи (одна строка)
        DMA1_Channel3->CCR |= DMA_CCR_EN;						//	включение канала
        display_flag.frame_buf_busy = 1;
    } else {
        return;
    }
}
//	Обработчик прерывания по завершении передачи массива в дисплей
void DMA1_Channel2_3_IRQHandler(void)
{
    //	Обработка прерывания по завершению передачи DMA1_Channel5 --> SPI2:
    if (DMA1->ISR & DMA_ISR_TCIF3) {
        DMA1->IFCR |= DMA_IFCR_CTCIF3;			// сброс флага прерывания по окончании передачи от 3 канала DMA
        DMA1_Channel3->CCR &= ~DMA_CCR_EN;		// отключение канала
        while (!(SPI_SR & SPI_SR_TXE));
        while (SPI_SR & SPI_SR_BSY);			// ожидание конца передачи
        TX_DISABLE;								// отключаем прием данных дисплеем
        display_flag.page_switch++;
        if (display_flag.page_switch == (SCREEN_H >> 3)) {
            display_flag.value = 0x00;
        }
        return;
    } else {
        return;
    }
}
#endif	//	DISPLAY_SPI1
#endif	//	STM32F0_SERIES
#endif	//	DMA_MODE


#ifdef STM32F1_SERIES
#ifdef DISPLAY_SPI1
void sh1106_SPI_init (void)
{
    //	тактирование альтернативных функций, GPIOA и GPIOC
    RCC->APB2ENR |= (RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_AFIOEN);
    //	PB0 - RESET
    GPIOB->CRL &= ~(GPIO_CRL_CNF0);     //	GPIO push-pull
    GPIOB->CRL |= (GPIO_CRL_MODE0_0);	//	10 MHz
    //	PA4 - CHIP ENABLE
    GPIOA->CRL &= ~(GPIO_CRL_CNF4);     //	GPIO push-pull
    GPIOA->CRL |= (GPIO_CRL_MODE4_0);	//	10 MHz
    //	PA5 - SCK
    GPIOA->CRL |= GPIO_CRL_MODE5_0;     //	10 MHz
    GPIOA->CRL &= ~GPIO_CRL_CNF5;
    GPIOA->CRL |= GPIO_CRL_CNF5_1;		//	Alt. func
    //	PA6 - SEND_DATA/SEND_CMD
    GPIOA->CRL &= ~(GPIO_CRL_CNF6);     //	GPIO push-pull
    GPIOA->CRL |= (GPIO_CRL_MODE6_0);	//	10 MHz
    //	PA7 - MOSI
    GPIOA->CRL |= GPIO_CRL_MODE7_0;     //	10 MHz
    GPIOA->CRL &= ~GPIO_CRL_CNF7;
    GPIOA->CRL |= GPIO_CRL_CNF7_1;		//	Alt. func.
    //	CPOL=0, CPHA=0, 8 bit frame, 4.5 MHz CLK:
    RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;	//	подать тактирование на модуль SPI1
    SPI1->CR1 = 0x0000;
    SPI1->CR2 = 0x0000;
    SPI1->CR1 |= (SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE | SPI_CR1_MSTR |
                  SPI_CR1_BR_1 | SPI_CR1_BR_0 | SPI_CR1_SSI | SPI_CR1_SSM);	//	2-wire state
    SPI1->CR1 |= SPI_CR1_SPE;			//	включить SPI
#ifdef DMA_MODE
    //	настройка источника и приемника:
    RCC->AHBENR |= RCC_AHBENR_DMA1EN;				//	тактирование DMA1
    DMA1_Channel3->CPAR = (uint32_t)&SPI_DR;		//	адрес регистра перефирии
    DMA1_Channel3->CMAR = (uint32_t)&frame_buf[0];	//	массив данных в памяти
    DMA1_Channel3->CNDTR = SCREEN_W;				//	количество байт для передачи (одна строка)
    //	инкрементация для перехода по элементам массива в памяти
    //	чтение из памяти, прерывание при передаче половины и окончании
    DMA1_Channel3->CCR = 0;
    DMA1_Channel3->CCR |= (DMA_CCR3_MINC | DMA_CCR3_DIR | DMA_CCR3_TCIE);
    SPI1->CR2 |= SPI_CR2_TXDMAEN;					//	разрешить работу SPI1 с DMA
    NVIC_SetPriority(DMA1_Channel3_IRQn, 15);		//	приоритет прерывания
    NVIC_EnableIRQ (DMA1_Channel3_IRQn);			//	разрешить прерывания от DMA, канал 3
#endif	//	DMA MODE
}

//	отключение перефирии, обеспечивающей работу дисплея
void sh1106_SPI_deinit (void)
{
    //	отключение тактирования переферии:
    NVIC_DisableIRQ (SPI1_IRQn);
    RCC->APB2ENR &= (uint32_t)~RCC_APB2ENR_SPI1EN;
#ifdef DMA_MODE
    NVIC_DisableIRQ (DMA1_Channel3_IRQn);
    RCC->AHBENR &= (uint32_t)~RCC_AHBENR_DMA1EN;
#endif	//	DMA MODE
    //	перевод всех выводов в состояние входа:
    //	PA6 - RESET
    GPIOA->CRL &= ~(GPIO_CRL_CNF6 | GPIO_CRL_MODE6);
    GPIOA->CRL |= (GPIO_CRL_CNF6_0);
    //	PC5 - CHIP ENABLE
    GPIOC->CRL &= ~(GPIO_CRL_CNF5 | GPIO_CRL_MODE5);
    GPIOC->CRL |= (GPIO_CRL_CNF5_0);
    //	PA5 - SCK
    GPIOA->CRL &= ~(GPIO_CRL_CNF5 | GPIO_CRL_MODE5);
    GPIOA->CRL |= (GPIO_CRL_CNF5_0);
    //	PC4 - SEND_DATA/SEND_CMD
    GPIOC->CRL &= ~(GPIO_CRL_CNF4 | GPIO_CRL_MODE4);
    GPIOC->CRL |= (GPIO_CRL_CNF4_0);
    //	PA7 - MOSI
    GPIOA->CRL &= ~(GPIO_CRL_CNF7 | GPIO_CRL_MODE7);
    GPIOA->CRL |= (GPIO_CRL_CNF7_0);
}
#endif	//	DISPLAY_SPI1

#ifdef DISPLAY_SPI2
void sh1106_SPI_init (void)
{
    //	тактирование альтернативных функций и GPIOB
    RCC->APB2ENR |= (RCC_APB2ENR_IOPBEN | RCC_APB2ENR_AFIOEN);
    //	PB11 - RESET
    GPIOB->CRH &= ~(GPIO_CRH_CNF11);	//	GPIO push-pull
    GPIOB->CRH |= (GPIO_CRH_MODE11_0);	//	10 MHz
    //	PB12 - CHIP ENABLE
    GPIOB->CRH &= ~(GPIO_CRH_CNF12);	//	GPIO push-pull
    GPIOB->CRH |= (GPIO_CRH_MODE12_0);	//	10 MHz
    //	PB13 - SCK
    GPIOB->CRH |= GPIO_CRH_MODE13_0;	//	10 MHz
    GPIOB->CRH &= ~GPIO_CRH_CNF13;
    GPIOB->CRH |= GPIO_CRH_CNF13_1;		//	Alt. func
    //	PB14 - SEND_DATA/SEND_CMD
    GPIOB->CRH &= ~(GPIO_CRH_CNF14);	//	GPIO push-pull
    GPIOB->CRH |= (GPIO_CRH_MODE14_0);	//	10 MHz
    //	PB15 - MOSI
    GPIOB->CRH |= GPIO_CRH_MODE15_0;	//	10 MHz
    GPIOB->CRH &= ~GPIO_CRH_CNF15;
    GPIOB->CRH |= GPIO_CRH_CNF15_1;		//	Alt. func.
    //	CPOL=0, CPHA=0, 8 bit frame, 3.0 MHz CLK:
    RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;	//	подать тактирование на модуль SPI2
    SPI2->CR1 = 0x0000;
    SPI2->CR2 = 0x0000;
    SPI2->CR1 |= (SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE | SPI_CR1_MSTR |
                  SPI_CR1_BR_1 | SPI_CR1_SSI | SPI_CR1_SSM);	//	2-wire state
    SPI2->CR1 |= SPI_CR1_SPE;			//	включить SPI
#ifdef DMA_MODE
    //	настройка источника и приемника:
    RCC->AHBENR |= RCC_AHBENR_DMA1EN;				//	тактирование DMA1
    DMA1_Channel5->CPAR = (uint32_t)&SPI_DR;		//	адрес регистра перефирии
    DMA1_Channel5->CMAR = (uint32_t)&frame_buf[0];	//	массив данных в памяти
    DMA1_Channel5->CNDTR = SCREEN_W;				//	количество байт для передачи (одна строка)
    //	инкрементация для перехода по элементам массива в памяти
    //	чтение из памяти, прерывание при передаче половины и окончании
    DMA1_Channel5->CCR = 0;
    DMA1_Channel5->CCR |= (DMA_CCR5_MINC | DMA_CCR5_DIR | DMA_CCR5_TCIE);
    SPI2->CR2 |= SPI_CR2_TXDMAEN;					//	разрешить работу SPI2 с DMA
//	NVIC_SetPriority(DMA1_Channel5_IRQn, X);		//	приоритет прерывания
    NVIC_EnableIRQ (DMA1_Channel5_IRQn);			//	разрешить прерывания от DMA, канал 5
#endif	//	DMA MODE
}

//	отключение перефирии, обеспечивающей работу дисплея
void sh1106_SPI_deinit (void)
{
    //	отключение тактирования переферии:
    NVIC_DisableIRQ (SPI2_IRQn);
    RCC->APB1ENR &= ~RCC_APB1ENR_SPI2EN;
#ifdef DMA_MODE
    NVIC_DisableIRQ (DMA1_Channel5_IRQn);
    RCC->AHBENR &= ~RCC_AHBENR_DMA1EN;
#endif	//	DMA MODE
    //	перевод всех выводов в состояние входа:
    //	PB11 - RESET
    GPIOB->CRH &= ~(GPIO_CRH_CNF11 | GPIO_CRH_MODE11);
    GPIOB->CRH |= (GPIO_CRH_CNF11_0);
    //	PB12 - CHIP ENABLE
    GPIOB->CRH &= ~(GPIO_CRH_CNF12 | GPIO_CRH_MODE12);
    GPIOB->CRH |= (GPIO_CRH_CNF12_0);
    //	PB13 - SCK
    GPIOB->CRH &= ~(GPIO_CRH_CNF13 | GPIO_CRH_MODE13);
    GPIOB->CRH |= (GPIO_CRH_CNF13_0);
    //	PB14 - SEND_DATA/SEND_CMD
    GPIOB->CRH &= ~(GPIO_CRH_CNF14 | GPIO_CRH_MODE14);
    GPIOB->CRH |= (GPIO_CRH_CNF14_0);
    //	PB15 - MOSI
    GPIOB->CRH &= ~(GPIO_CRH_CNF15 | GPIO_CRH_MODE15);
    GPIOB->CRH |= (GPIO_CRH_CNF15_0);
}
#endif	//	DISPLAY_SPI2
#endif	//	STM32F1_SERIES


#ifdef DMA_MODE
#ifdef STM32F1_SERIES
#ifdef DISPLAY_SPI1
//	Запуск пересылки массива данных в дисплей
void sh1106_DMA_send (void)
{
    // передаем данные если модуль DMA был отключен и установлен флаг готовности буфера кадра:
    if (!(DMA1_Channel3->CCR & DMA_CCR3_EN) && display_flag.frame_buf_ready) {
        SEND_CMD;
        TX_ENABLE;
        sh1106_send_byte(0xB0 | display_flag.page_switch);		//	адрес страницы
        sh1106_send_byte(0x10);									//	смещение
        sh1106_send_byte(0x02);									//	на 2 колонки
        while (SPI_SR & SPI_SR_BSY);
        SEND_DATA;												// режим передачи данных
        DMA1_Channel3->CMAR = (uint32_t)&frame_buf[SCREEN_W * display_flag.page_switch];
        DMA1_Channel3->CNDTR = SCREEN_W;						// количество байт для передачи (одна строка)
        DMA1_Channel3->CCR |= DMA_CCR3_EN;						// включение канала
        display_flag.frame_buf_busy = 1;
    } else {
        return;
    }
}
//	Обработчик прерывания по завершении передачи массива в дисплей
void DMA1_Channel3_IRQHandler (void)
{
    //	Обработка прерывания по завершению передачи DMA1_Channel3 --> SPI1:
    if (DMA1->ISR & DMA_ISR_TCIF3) {
        DMA1->IFCR |= DMA_IFCR_CTCIF3;                  // сброс флага прерывания по окончании передачи от 3 канала DMA
        DMA1_Channel3->CCR &= (uint32_t)~DMA_CCR3_EN;	// отключение канала
        while (!(SPI_SR & SPI_SR_TXE));
        while (SPI_SR & SPI_SR_BSY);                    // ожидание конца передачи
        TX_DISABLE;                                     // отключаем прием данных дисплеем
        display_flag.page_switch++;
        if (display_flag.page_switch == (SCREEN_H >> 3)) {
            display_flag.value = 0x00;
        }
        return;
    }
}
#endif	//	DISPLAY_SPI1

#ifdef DISPLAY_SPI2
//	Запуск пересылки массива данных в дисплей
void sh1106_DMA_send (void)
{
    // передаем данные если модуль DMA был отключен и установлен флаг готовности буфера кадра:
    if (!(DMA1_Channel5->CCR & DMA_CCR5_EN) && display_flag.frame_buf_ready) {
        SEND_CMD;
        TX_ENABLE;
        sh1106_send_byte(0xB0 | display_flag.page_switch);		//	адрес страницы
        sh1106_send_byte(0x10);									//	смещение
        sh1106_send_byte(0x02);									//	на 2 колонки
        while (SPI_SR & SPI_SR_BSY);
        SEND_DATA;												// режим передачи данных
        DMA1_Channel5->CMAR = (uint32_t)&frame_buf[SCREEN_W * display_flag.page_switch];
        DMA1_Channel5->CNDTR = SCREEN_W;						// количество байт для передачи (одна строка)
        DMA1_Channel5->CCR |= DMA_CCR5_EN;						// включение канала
        display_flag.frame_buf_busy = 1;
    } else {
        return;
    }
}
//	Обработчик прерывания по завершении передачи массива в дисплей
void DMA1_Channel5_IRQHandler (void)
{
    //	Обработка прерывания по завершению передачи DMA1_Channel5 --> SPI2:
    if (DMA1->ISR & DMA_ISR_TCIF5) {
        DMA1->IFCR |= DMA_IFCR_CTCIF5;			// сброс флага прерывания по окончании передачи от 5 канала DMA
        DMA1_Channel5->CCR &= ~DMA_CCR5_EN;		// отключение канала
        while (!(SPI_SR & SPI_SR_TXE));
        while (SPI_SR & SPI_SR_BSY);			// ожидание конца передачи
        TX_DISABLE;								// отключаем прием данных дисплеем
        display_flag.page_switch++;
        if (display_flag.page_switch == (SCREEN_H >> 3)) {
            display_flag.value = 0x00;
        }
        return;
    }
}
#endif	//	DISPLAY_SPI2
#endif	//	STM32F1_SERIES
#endif	//	DMA_MODE


//	Отправка одного байта
void sh1106_send_byte (uint8_t data)
{
    SPI_DR = data;
    while (!(SPI_SR & SPI_SR_TXE));
}

//	Инициализация дисплея
void sh1106_start_init (void)
{
    display_flag.value = 0;				//	сброс всех флагов и счетчика
    RESET_ASSERT;						//	прижимаем линию сброса к земле, сброс контроллера дисплея
    TX_DISABLE;							//	отключаем прием дисплея
    SEND_CMD;							//	настраиваем передачу команды
    for (uint16_t i = 0; i < 1000; i++);//	не менее 13 мкс в состоянии сброса
    RESET_DEASSERT;						//	подаем питание на линию сброса
    for (uint16_t i = 0; i < 1000; i++);//	не менее 13 мкс
    TX_ENABLE;							//	включаем передачу

    sh1106_send_byte(0xAE);		//	OFF
    sh1106_send_byte(0x40);		//	set display start line
    sh1106_send_byte(0xB0);		//	set page address
    sh1106_send_byte(0x81);		//	set contrast
    sh1106_send_byte(0x7F);		//	value contrast
    sh1106_send_byte(0xA1);		//	set segment re-map: left rotation
//  sh1106_send_byte(0xA0);		//	set segment re-map: right rotation (use for flipped position on PCB)
    sh1106_send_byte(0xA4);		//	0xA4 - RAM, 0xA5 - ignore RAM
    sh1106_send_byte(0xA6);		//	0xA6 - normal, 0xA7 - inverting pixels
    sh1106_send_byte(0xA8);		//	set multiplex ratio
    sh1106_send_byte(0x3F);		//	value multiplex ratio
    sh1106_send_byte(0xC8);		//	set common output scan direction: COM[N-1] to COM0
//  sh1106_send_byte(0xC0);     //	set common output scan direction: COM0 to COM[N-1] (use for flipped position on PCB)
    sh1106_send_byte(0xD3);		//	set display dffset
    sh1106_send_byte(0x00);		//	value display dffset
    sh1106_send_byte(0xD5);		//	set display divide ratio / osc
    sh1106_send_byte(0x80);		//	value ratio / osc
    sh1106_send_byte(0xDA);		//	common pads hardware configuration
    sh1106_send_byte(0x12);		//	= progressive
    sh1106_send_byte(0x10);		//	start column address HIGH
    sh1106_send_byte(0x02);		//	start column address LOW
    sh1106_send_byte(0xDB);		//	VCOM deselect level mode set
    sh1106_send_byte(0x40);		//	value VCOM data set
    sh1106_send_byte(0xAF);		//	ON

    while (SPI_SR & SPI_SR_BSY);
    TX_DISABLE;
}

//	Настройка яркости
void sh1106_set_contrast (uint8_t value)
{
    #ifdef DMA_MODE
    if (display_flag.value)
        return;
    #endif

    SEND_CMD;
    TX_ENABLE;
    sh1106_send_byte(0x81);		//	set contrast control
    sh1106_send_byte(value);	//	contrast value
    while (SPI_SR & SPI_SR_BSY);
    TX_DISABLE;
}

//	перевод дисплея в режим сна
void sh1106_standby (void)
{
    while (SPI_SR & SPI_SR_BSY);//	ждем окончания передачи
    TX_DISABLE;
    SEND_CMD;
    TX_ENABLE;
    sh1106_send_byte(0xAE);		//	display OFF
    while (SPI_SR & SPI_SR_BSY);//	ждем окончания передачи
    TX_DISABLE;
    RESET_ASSERT;
}
