/**
 * @file		pcd8544.с
 * @author		Chugay E.A.
 * @version		0.02
 * @date		11.04.2021
 *
 * @brief       Функции, обеспечивающие инициализацию перефирийных модулей МК,
 *              инициализацию контроллера PCD8544 и их совместную работу.
 * @details
 * File used UTF-8 code page. Don't convert other codepage for correct work.
 */

/*------------------------------------------------------------------ Includes */
#include "pcd8544.h"

/*-------------------------------------------------------- External variables */
extern volatile uint8_t frame_buf[BUF_SIZE];

/*--------------------------------------------------------- Private variables */
static union {
    struct {
        volatile uint8_t frame_buf_busy	 :1;	//  Флаг равен 1 при передаче кадра в дисплей
        volatile uint8_t frame_buf_ready :1;	//  Флаг должен быть установлен в 1 после окончания формирования кадра
        volatile uint8_t page_switch	 :6;	//  номер передаваемой страницы буфера
    };
    volatile uint8_t value;
} display_flag;

/*----------------------------------------------- Private function prototypes */
/**
 * @brief	Инициализация SPI для работы с PCD8544
 * @param	Нет
 * @retval	Нет
 */
void pcd8544_SPI_init (void);

/**
 * @brief	Отключение SPI, DMA, и перевод выводов в плавающий вход
 * @param	Нет
 * @retval	Нет
 */
void pcd8544_SPI_deinit (void);

/**
 * @brief	Стартовая инициализация контроллера дисплея
 * @param	Нет
 * @retval	Нет
 */
void pcd8544_start_init (void);

/**
 * @brief	Настройка яркости дисплея (0..255)
 * @param	value - новое значение контрастности
 * @retval	Нет
 */
void pcd8544_set_contrast (uint8_t value);

/**
 * @brief	Запуск передачи буфера в дисплей
 * @param	Нет
 * @retval	Нет
 */
void pcd8544_DMA_send (void);

/**
 * @brief	Передача команды отключения дисплея
 * @param	Нет
 * @retval	Нет
 */
void pcd8544_standby (void);

/**
 * @brief	Отправка одного байта в дисплей
 * @param	data - передаваемый байт
 * @retval	Нет
 */
void pcd8544_send_byte (uint8_t data);



/*----------------------------------------------------------------------------*/
/*---------------------------------------------------------------- Functions: */
/*------------------------------------------------------------------ Функции: */
/*----------------------------------------------------------------------------*/

//  Инициализация дисплея
void init_display (void)
{
    pcd8544_SPI_init();
    pcd8544_start_init();
}

//  Де-инициализация дисплея
void deinit_display (void)
{
    pcd8544_standby();
    pcd8544_SPI_deinit();
}

//  Настройка контрастности дисплея
void display_set_contrast (uint8_t value)
{
    pcd8544_set_contrast(value);
}

//  передача кадра в дисплей
void send_frame_to_display (uint8_t signal, void(*frame)(uint8_t))
{
    if (!display_flag.frame_buf_busy) {
        frame_clear_buf();
        (*frame)(signal);
        display_flag.frame_buf_ready = 1;
    }
    pcd8544_DMA_send();
}



/*----------------------------------------------------------------------------*/
/*--------------------------------------------------------- Private function: */
/*------------------------------------------------------- Внутренние функции: */
/*----------------------------------------------------------------------------*/

#ifdef STM32F0_SERIES
#ifdef DISPLAY_SPI1
void pcd8544_SPI_init(void)
{
    RCC->AHBENR |= (RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN);
    //	PB1 - RESET
    GPIOB->MODER   &= ~GPIO_MODER_MODER1;		//	input
    GPIOB->OTYPER  &= ~GPIO_OTYPER_OT_1;		//	push/pull
    GPIOB->PUPDR   &= ~GPIO_PUPDR_PUPDR1;		//	no pull-up or pull-down
    GPIOB->MODER   |= GPIO_MODER_MODER1_0;		//	output
    GPIOB->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR1;	//	high speed
    //	PA6 - CHIP ENABLE
    GPIOA->MODER   &= ~GPIO_MODER_MODER6;		//	input
    GPIOA->OTYPER  &= ~GPIO_OTYPER_OT_6;		//	push/pull
    GPIOA->PUPDR   &= ~GPIO_PUPDR_PUPDR6;		//	no pull-up or pull-down
    GPIOA->MODER   |= GPIO_MODER_MODER6_0;		//	output
    GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR6;	//	high speed
    //	PA4 - SEND_DATA/SEND_CMD
    GPIOA->MODER   &= ~GPIO_MODER_MODER4;		//	input
    GPIOA->OTYPER  &= ~GPIO_OTYPER_OT_4;		//	push/pull
    GPIOA->PUPDR   &= ~GPIO_PUPDR_PUPDR4;		//	no pull-up or pull-down
    GPIOA->MODER   |= GPIO_MODER_MODER4_0;		//	output
    GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR4;	//	high speed
    //	PA7 - MOSI
    GPIOA->MODER  &= ~GPIO_MODER_MODER7;		//	input
    GPIOA->OTYPER &= ~GPIO_OTYPER_OT_7;			//	push/pull
    GPIOA->PUPDR  &= ~GPIO_PUPDR_PUPDR7;		//	no pull-up or pull-down
    GPIOA->MODER  |= GPIO_MODER_MODER7_1;		//	alternate func.
    GPIOA->AFR[0] &= ~GPIO_AFRL_AFSEL7;			//	SPI1 MOSI
    //	PA5 - SCK
    GPIOA->MODER  &= ~GPIO_MODER_MODER5;		//	input
    GPIOA->OTYPER &= ~GPIO_OTYPER_OT_5;			//	push/pull
    GPIOA->PUPDR  &= ~GPIO_PUPDR_PUPDR5;		//	no pull-up or pull-down
    GPIOA->MODER  |= GPIO_MODER_MODER5_1;		//	alternate func.
    GPIOA->AFR[0] &= ~GPIO_AFRL_AFSEL5;			//	SPI1 SCK
    //	CPOL=0, CPHA=0, 8 bit frame, 3.0 MHz CLK:
    RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;			//	подать тактирование на модуль SPI2
    SPI1->CR1 = 0x0000;
    SPI1->CR2 = 0x0000;
    SPI1->CR1 |= (SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE | SPI_CR1_MSTR |
                  SPI_CR1_BR_1 | SPI_CR1_BR_0 | SPI_CR1_SSI | SPI_CR1_SSM);	//	2-wire state
    SPI1->CR1 |= SPI_CR1_SPE;					//	включить SPI
#ifdef DMA_MODE
    //	настройка источника и приемника:
    RCC->AHBENR |= RCC_AHBENR_DMAEN;				//	тактирование DMA
    DMA1_Channel3->CPAR = (uint32_t)&SPI_DR;		//	адрес регистра перефирии
    DMA1_Channel3->CMAR = (uint32_t)&frame_buf[0];	//	массив данных в памяти
    DMA1_Channel3->CNDTR = SCREEN_W;				//	количество байт для передачи (одна строка)
    //	инкрементация для перехода по элементам массива в памяти
    //	чтение из памяти, прерывание при передаче половины и окончании
    DMA1_Channel3->CCR = 0;
    DMA1_Channel3->CCR |= (DMA_CCR_MINC | DMA_CCR_DIR | DMA_CCR_TCIE);
    SPI1->CR2 |= SPI_CR2_TXDMAEN;					//	разрешить работу SPI2 с DMA
//	NVIC_SetPriority(DMA1_Channel2_3_IRQn, X);		//	приоритет прерывания
    NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);			//	разрешить прерывания от DMA, канал 3
#endif	//	DMA MODE
}

//	отключение перефирии, обеспечивающей работу дисплея
void pcd8544_SPI_deinit(void)
{
    NVIC_DisableIRQ (SPI1_IRQn);
    RCC->APB2ENR &= ~RCC_APB2ENR_SPI1EN;
#ifdef DMA_MODE
    NVIC_DisableIRQ (DMA1_Channel2_3_IRQn);
    RCC->AHBENR &= ~RCC_AHBENR_DMAEN;
#endif	//	DMA MODE
    GPIOB->MODER &= ~GPIO_MODER_MODER1;		//	input	PB1 - RESET
    GPIOA->MODER &= ~GPIO_MODER_MODER6;		//	input	PA6 - CHIP ENABLE
    GPIOA->MODER &= ~GPIO_MODER_MODER4;		//	input	PA4 - SEND_DATA/SEND_CMD
    GPIOA->MODER &= ~GPIO_MODER_MODER7;		//	input	PA7 - MOSI
    GPIOA->MODER &= ~GPIO_MODER_MODER5;		//	input	PA5 - SCK
}
#endif	//	DISPLAY_SPI1
#endif	//	STM32F0_SERIES

#ifdef DMA_MODE
#ifdef STM32F0_SERIES
#ifdef DISPLAY_SPI1
//	Запуск пересылки массива данных в дисплей
void pcd8544_DMA_send(void)
{
    // передаем данные если модуль DMA был отключен и установлен флаг готовности буфера кадра:
    if (!(DMA1_Channel3->CCR & DMA_CCR_EN) && display_flag.frame_buf_ready) {
        SEND_DATA;								// режим передачи данных
        TX_ENABLE;
        DMA1_Channel3->CNDTR = BUF_SIZE;		// количество байт для передачи
        DMA1_Channel3->CCR |= DMA_CCR_EN;		// включение канала
        display_flag.frame_buf_busy = 1;
    } else {
        return;
    }
}
//	Обработчик прерывания по завершении передачи массива в дисплей
void DMA1_Channel2_3_IRQHandler(void)
{
    if (DMA1->ISR & DMA_ISR_TCIF3) {
        DMA1->IFCR |= DMA_IFCR_CTCIF3;			// сброс флага прерывания по окончании передачи от 3 канала DMA
        DMA1_Channel3->CCR &= ~DMA_CCR_EN;		// отключение канала
        while (!(SPI_SR & SPI_SR_TXE));
        while (SPI_SR & SPI_SR_BSY);			// ожидание конца передачи
        TX_DISABLE;								// отключаем прием данных дисплеем
        display_flag.value = 0x00;				// буфер готов к модификации
        return;
    } else {
        return;
    }
}
#endif	//	DISPLAY_SPI1
#endif	//	STM32F0_SERIES
#endif	//	DMA_MODE

//	Отправка одного байта
void pcd8544_send_byte(uint8_t data)
{
    SPI_DR = data;
    while(!(SPI_SR & SPI_SR_TXE));
}

//	Инициализация дисплея
void pcd8544_start_init (void)
{
	display_flag.value = 0;				//	сброс всех флагов и счетчика
	TX_DISABLE;							//	отключаем прием дисплея
	RESET_ASSERT;						//	прижимаем линию сброса к земле, сброс контроллера дисплея
    SEND_CMD;							//	настраиваем передачу команды
	TX_ENABLE;							//	включаем передачу
	for (uint8_t i=0; i<6; i++);		//	тянем время более 100 nsec.
	RESET_DEASSERT;						//	подаем питание на линию сброса
	
	pcd8544_send_byte(0x21);
	pcd8544_send_byte(0x13);
	pcd8544_send_byte(0x04);
	pcd8544_send_byte(0xB8);
	pcd8544_send_byte(0x20);
	pcd8544_send_byte(0x0C);

	while (SPI_SR & SPI_SR_BSY);
	TX_DISABLE;							//	отключаем прием дисплея
}

//	Настройка яркости
void pcd8544_set_contrast(uint8_t value)
{
    while(1);
}

//	перевод дисплея в режим сна
void pcd8544_standby(void)
{
    while (1);
}



/*
//	Настройка положения курсора: строка и байт в строке
inline void pcd8544_set_cursor (uint8_t str_pos, uint8_t byte_pos)
{
	if ((str_pos > 5) || (byte_pos > 83)) return;	//	проверка попадания значений в диапазоны 0..5 и 0..83
	SEND_CMD;							//	режим передачи команды
	TX_ENABLE;							//	включаем прием данных дисплеем
	SPI_DR = (0x40 | str_pos);			//	отправка номера строки
	while (!(SPI_SR & SPI_SR_TXE));
	SPI_DR = (0x80 | byte_pos);				//	отправка номера байта в строке
	while (!(SPI_SR & SPI_SR_TXE));
	while (SPI_SR & SPI_SR_BSY);
	TX_DISABLE;							//	отключаем прием данных дисплеем
}

//	Очистка всего дисплея
void pcd8544_clear (void)
{
	pcd8544_set_cursor(0, 0);			// устанавливаем курсор в позицию 0, 0.
	SEND_DATA;							// режим передачи данных
	TX_ENABLE;							// включаем прием данных дисплеем
	for (uint16_t i=0; i<504; i++) {
		SPI_DR = 0x00;					// заполнение нулями матрицы дисплея
		while (!(SPI_SR & SPI_SR_TXE));
	}
	while (SPI_SR & SPI_SR_BSY);
	TX_DISABLE;							// отключаем прием дисплея
}
*/