/**
 * @file		widget.с
 * @author		Chugay E.A.
 * @version		0.01 - base version.
 *              0.02 - widget_menu_list(...) - add scrollbar.
 *              0.03 - Исправлены виджеты с учетом изменения шрифта "font_16h":
 *                   - изменена функция widget_build_number().
 * @date		30.03.2020
 * @brief		Виджеты для монохромного дисплея.
 * @details
 * File used UTF-8 code page. Don't convert other codepage for correct work.
 */

/*----------------------------------------------------------------- Includes: */
#include "widget.h"

/*--------------------------------------------------------- Global variables: */
//char widget_buf[WIDGET_BUF_SIZE];

/*------------------------------------------------------- External variables: */
extern const void *current_font;
extern volatile uint8_t frame_buf[];

/*-------------------------------------------------------- Private variables: */
static const char day_in_month[12] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

/*---------------------------------------------- Private functions prototype: */
/**
 * @brief	Проверка высоты строки, в зависимости от выбранного шрифта.
 * @param   str_h - высота строки, указанная как выбранная.
 * @retval	Высота строки, 8 или 16.
 */
static uint8_t check_h (uint8_t str_h);

/**
 * @brief	Изменение символа в позиции курсора, вызывается в widget_build_string().
 *          Корректировать функцию для иных диапазонов символов.
 * @param   *buf - подготовленный массив строки.
 * @param	cursor - позиция курсора, только четные значения, включая 0.
 * @param   incr - сигнал инкрементации символа.
 * @param   decr - сигнал декрементации символа.
 * @retval	Нет.
 */
static void widget_symbol_select (char *buf, uint8_t cursor, uint8_t incr, uint8_t decr);

/*--------------------------------------------------------- Public functions: */
//	Левая кнопка
void widget_btn_l (const char *str, uint8_t invert)
{
    uint8_t w = (SCREEN_W >> 2) + (SCREEN_W >> 3);
    uint8_t l = (uint8_t)frame_text_string_width(str);

    if (l > w) return;
    frame_text_string(((w >> 1) - (l >> 1)), 0, str);
    if (invert) frame_invert_rect(0, 0, (w - 1), 8);
}


//	Правая кнопка
void widget_btn_r (const char *str, uint8_t invert)
{
    uint8_t w = (SCREEN_W >> 2) + (SCREEN_W >> 3);
    uint8_t l = (uint8_t)frame_text_string_width(str);

    if (l > w) return;
    frame_text_string((SCREEN_W - (w >> 1) - (l >> 1) + 1), 0, str);
    if (invert) frame_invert_rect((SCREEN_W - w), 0, (SCREEN_W - 1), 8);
}


//	Средняя кнопка
void widget_btn_m (const char *str, uint8_t invert)
{
    uint8_t w = (SCREEN_W >> 2);
    uint8_t l = (uint8_t)frame_text_string_width(str);

    if (l > w) return;
    frame_text_string(((SCREEN_W >> 1) - (l >> 1)), 0, str);
    if (invert) frame_invert_rect(((SCREEN_W >> 1) - (w >> 1)), 0, ((SCREEN_W >> 1) + (w >> 1) - 1), 8);
}


//	Кнопка [/\]
void widget_btn_up (const char symbol, uint8_t invert)
{
/*
    //  Если SCREEN_W >= 96. Построение стрелки вверх линиями
    frame_draw_line(((SCREEN_W >> 1) - (SCREEN_W >> 4) - 6), 2, ((SCREEN_W >> 1) - (SCREEN_W >> 4) - 1), 7);
    frame_draw_line(((SCREEN_W >> 1) - (SCREEN_W >> 4) - 6), 1, ((SCREEN_W >> 1) - (SCREEN_W >> 4) - 1), 6);
    frame_draw_line(((SCREEN_W >> 1) - (SCREEN_W >> 4)), 7, ((SCREEN_W >> 1) - (SCREEN_W >> 4) + 5), 2);
    frame_draw_line(((SCREEN_W >> 1) - (SCREEN_W >> 4)), 6, ((SCREEN_W >> 1) - (SCREEN_W >> 4) + 5), 1);
*/
    //  Если SCREEN_W >= 64. Вывод ASCII символа треугольника вверх
    frame_text_char(((SCREEN_W >> 1) - (SCREEN_W >> 4) - 3), 0, symbol);      //  0x1E == [/\]

    if (invert) frame_invert_rect(((SCREEN_W >> 2) + (SCREEN_W >> 3)), 0, ((SCREEN_W >> 1) - 1), 8);
}


//	Кнопка [\/]
void widget_btn_down (const char symbol, uint8_t invert)
{
/*
    //  Если SCREEN_W >= 96. Построение стрелки в линиями
    frame_draw_line(((SCREEN_W >> 1) + (SCREEN_W >> 4) - 6), 7, ((SCREEN_W >> 1) + (SCREEN_W >> 4) - 1), 2);
    frame_draw_line(((SCREEN_W >> 1) + (SCREEN_W >> 4) - 6), 6, ((SCREEN_W >> 1) + (SCREEN_W >> 4) - 1), 1);
    frame_draw_line(((SCREEN_W >> 1) + (SCREEN_W >> 4)), 2, ((SCREEN_W >> 1) + (SCREEN_W >> 4) + 5), 7);
    frame_draw_line(((SCREEN_W >> 1) + (SCREEN_W >> 4)), 1, ((SCREEN_W >> 1) + (SCREEN_W >> 4) + 5), 6);
*/
    //  Если SCREEN_W >= 64. Вывод ASCII символа треугольника вниз
    frame_text_char(((SCREEN_W >> 1) + (SCREEN_W >> 4) - 4), 0, symbol);      //  0x1F == [\/]

    if (invert) frame_invert_rect((SCREEN_W >> 1), 0, ((SCREEN_W >> 1) + (SCREEN_W >> 3) - 1), 8);
}


//  Прогрессбар
void widget_progressbar (uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint32_t crnt, uint32_t max, uint8_t orient)
{
    if (crnt > max) crnt = max;

    if (crnt == max) {
        frame_draw_rect (x1, y1, x2, y2, FILL);
    } else {
        frame_draw_rect (x1, y1, x2, y2, UNFILL);
        if (orient == WIDGET_HORIZONTAL) {
            frame_draw_rect (x1, y1, (x1 + (crnt * (x2 - x1)) / max), y2, FILL);
        } else if (orient == WIDGET_VERTICAL) {
            frame_draw_rect (x1, y1, x2, (y1 + (crnt * (y2 - y1)) / max), FILL);
        }

    }
}


//  Вывод сообщения в центр экрана
void widget_message (const char **str, uint8_t str_q, uint8_t str_h)
{
    //  Выход, если используется слишком высокий шрифт
    if (((*((uint8_t*)current_font)) >> 4) > 1) return;

    //  Проверка корректного значения высоты строки
    str_h = check_h(str_h);

    //	Ограничение количества отображаемых строк при необходимости
    if ((str_q * str_h) > SCREEN_H) {
        str_q = ((str_h == 16) ? (SCREEN_H >> 4) : (SCREEN_H >> 3));
    }

    //	Вывод строк сообщения
    uint16_t w = 0;
    uint8_t  s = ((SCREEN_H - (str_q * str_h)) >> 1) + ((str_q - 1) * str_h);

    if ((*((uint8_t*)current_font)) >> 4) {         //  Если текущий шрифт 16 пикс.
        for (uint8_t i = 0; i < str_q; i++) {
            w = frame_text_string_width(str[i]);
            if (w > SCREEN_W) continue;
            frame_text_string(((SCREEN_W - w) >> 1), (s - (i << 4)), str[i]);
        }
    } else if (str_h == 16) {                       //  Масштабирование шрифта высотой 8 пикс.
        for (uint8_t i = 0; i < str_q; i++) {
            w = (frame_text_string_width(str[i]) << 1);
            if (w > SCREEN_W) continue;
            frame_text_string(((SCREEN_W - w) >> 2), (s - (i << 4)), str[i]);
            frame_string_scale(((SCREEN_H - (s - (i << 4))) >> 3) - 1);
        }
    } else {                                        //  Строками высотой 8 пикс.
        for (uint8_t i = 0; i < str_q; i++) {
            w = frame_text_string_width(str[i]);
            if (w > SCREEN_W) continue;
            frame_text_string(((SCREEN_W - w) >> 1), (s - (i << 3)), str[i]);
        }
    }
}


//	Вывод на экран строкового меню
void widget_menu_list (const char **str, uint8_t max_pos, uint8_t str_h, uint8_t cursor)
{
    if (((*((uint8_t*)current_font)) >> 4) > 1) return; //  Выход, если используется слишком высокий шрифт

    static uint8_t offset = 0;		//	Определяет, с какого элемента массива строк начинать отображение
    uint8_t str_q = 0;				//	Количество строк, помещающихся на экран

    #ifdef USE_SCROLLBAR
    uint8_t bar_p = cursor;         //  Сохранение позиции курсора для отрисовки скроллбара в меню
    #endif

    //  Проверка корректного значения высоты строки
    str_h = check_h(str_h);

    //	Вычисление количества отображаемых строк
    if ((str_h == 16) && !(SCREEN_H & 0x0F)) {
        str_q = ((SCREEN_H - 16) >> 4) - 1;
    } else {
        str_q = ((SCREEN_H - 16) >> 3) - 1;
        str_h = 8;
    }

    //	Если длина списка меньше количества помещающихся на экране строк
    if (max_pos <= str_q) {
        str_q = max_pos;
    }

    //	Вычисление смещения экрана относительно массива строк (списка)
    if (cursor == 0) {                          //	из конца в начало
        offset = 0;
    } else if (cursor == max_pos) {             //	из начала в конец
        offset = max_pos - str_q;
    } else if (cursor < offset) {               //	перемещение вверх по списку
        offset = cursor;
    } else if (cursor > (offset + str_q)) {     //	перемещение вних по списку
        offset = cursor - str_q;
    }
/*
    Старый код, давал засветку курсора выше отображаемых строк меню при max_pos == 3.
    Этот эффект давало первое выполняемое условие.
    if ((max_pos - str_q) < offset) {
        offset = max_pos - str_q;
    } else if (cursor == 0) {                   //	из конца в начало
        offset = 0;
    } else if (cursor == max_pos) {             //	из начала в конец
        offset = max_pos - str_q;
    } else if (cursor < offset) {               //	перемещение вверх по списку
        offset = cursor;
    } else if (cursor > (offset + str_q)) {     //	перемещение вних по списку
        offset = cursor - str_q;
    }
*/
    //	Вывод нужных строк меню
    for (uint8_t i = 0; ((i <= str_q) && ((i + offset) <= max_pos)); i++) {
        frame_text_string(STRING_BEGIN, (SCREEN_H - 8 - (str_h * (i + 1))), str[offset + i]);
    }

    //	Выделение активной строки
    cursor = (cursor - offset);

    #ifdef MENU_STYLE_INVERT
    if (str_h == 8) {
        frame_invert_rect(0, (SCREEN_H - 16 - (str_h * cursor)), (SCREEN_W - 1), (SCREEN_H + str_h - 16 - (str_h * cursor)));
    } else {
        frame_invert_rect(0, (SCREEN_H - 24 - (str_h * cursor)), (SCREEN_W - 1), (SCREEN_H + str_h - 25 - (str_h * cursor)));
    }
    #endif	//	MENU_STYLE_INVERT

    #ifdef MENU_STYLE_ARROW
    if (str_h == 8) {
        frame_text_char(0, (SCREEN_H - 16 - (cursor << 3)), MARKER);
    } else {
        frame_text_char(0, (SCREEN_H - 24 - (cursor << 4)), MARKER);
    }
    #endif	//	MENU_STYLE_ARROW

    //  Масштабирование строк
    if ((str_h == 16) && !((*((uint8_t*)current_font)) >> 4)) {
        for (uint8_t i = 0; i < ((SCREEN_H >> 4) - 1); i++) {
            frame_string_scale(((SCREEN_H >> 3) - 2) - (i << 1));
        }
    }

    //  Полоса прокрутки в правой части экрана:
    #ifdef USE_SCROLLBAR
    uint8_t bar_h = (SCREEN_H - 16) / (max_pos + 1);
    uint8_t bar_c = SCREEN_H - (bar_h * (max_pos + 1)) - 16;
    frame_clear_rect((SCREEN_W - 4), 8, (SCREEN_W - 1), (SCREEN_H - 9));
    frame_draw_line((SCREEN_W - 2), 8, (SCREEN_W - 2), (SCREEN_H - 9));
    frame_invert_rect((SCREEN_W - 3), (bar_h * (max_pos - bar_p) + 8), (SCREEN_W - 1), (bar_h * (max_pos - bar_p + 1) + 7 + bar_c));
    #endif
}


//	Ввод числа
void widget_build_number (char *buf, uint8_t size, uint8_t str_h, uint8_t dot_pos, uint8_t cursor, uint8_t incr, uint8_t decr)
{
    //  Выход, если используется слишком высокий шрифт
    if (((*((uint8_t*)current_font)) >> 4) > 1) return;

    //  Некорректная позиция курсора
    if (cursor >= size) cursor = (size - 1);

    //  Проверка корректного значения высоты строки
    str_h = check_h(str_h);

    //  Ширина места для одной цифры
    uint8_t w = frame_text_char_width('0');
    (str_h == 8) ? (w += 2) : (w += 3);

    //  Вычисление координат начала вывода символов
    uint16_t y = (SCREEN_H - str_h) >> 1;
    uint16_t x = size * w;
    if (dot_pos && (dot_pos < size)) {
        x += ((w >> 2) + frame_text_char_width('.'));
        dot_pos = size - dot_pos;
    } else {
        dot_pos = size;
    }

    //  Проверка возможности разместить виджет на экране
    if ((str_h == 16) && !((*((uint8_t*)current_font)) >> 4)) {
        if ((x << 1) > SCREEN_W) {
            frame_text_char(0, y, 'E');
            return;
        }
        x = (SCREEN_W - (x << 1)) >> 2;
    } else {
        if (x > SCREEN_W) {
            frame_text_char(0, y, 'E');
            return;
        }
        x = (SCREEN_W - x) >> 1;
    }

    //  Изменение цифры под курсором
    if (incr) {
        (buf[cursor] < 9) ? (buf[cursor]++) : (buf[cursor] = 0);
    } else if (decr) {
        (buf[cursor] > 0) ? (buf[cursor]--) : (buf[cursor] = 9);
    }

    //  Вывод цифр и курсора
    if ((*((uint8_t*)current_font)) >> 4) {         //  Если текущий шрифт 16 пикс.
        for (uint8_t i = 0; i < size; i++) {
            if ((i == dot_pos) && dot_pos) {
                frame_text_char((x + 3), y, '.');
                x += ((w >> 2) + frame_text_char_width('.'));
            }
            if (cursor == i) frame_draw_rect(x, (y - 2), (x + w), (y + 17), 0);
            frame_text_char((x + 3), y, (0x30 + buf[i]));
            x += w;
        }
    } else {                                        //  Если текущий шрифт 8 пикс.
        uint8_t l, r;
        for (uint8_t i = 0; i < size; i++) {
            if ((i == dot_pos) && dot_pos) {
                frame_text_char((x + 1), y, '.');
                x += ((w >> 2) + frame_text_char_width('.'));
            }
            if (cursor == i) {
                if (str_h == 16) {                  //  Если будет масштабирование строки то вычисляем края курсора
                    l = (x << 1) + 1;
                    r = (x + w - 1) << 1;
                } else {                            //  Без масштабирования отрисовываем курсор сразу
                    frame_draw_rect(x, (y - 1), (x + w), (y + 9), 0);
                }
            }
            if (buf[i] == 1) {
                frame_text_char((x + 3), y, (0x30 + buf[i]));
            } else {
                frame_text_char((x + 2), y, (0x30 + buf[i]));
            }
            x += w;
        }
        if (str_h == 16) {                          //  Масштабирование строки и отрисовка курсора
            frame_string_scale(((SCREEN_H - y) >> 3) - 1);
            if (cursor < size) frame_draw_rect(l, (y - 1), r, (y + 18), 0);
        }
    }
}


//  Ввод даты
void widget_build_date (char *buf, uint8_t str_h, uint8_t cursor, uint8_t incr, uint8_t decr)
{
    //  Выход, если используется слишком высокий шрифт
    if (((*((uint8_t*)current_font)) >> 4) > 1) return;

    //  Некорректная позиция курсора
    if (cursor > 2) cursor = 2;

    //  Проверка корректного значения высоты строки
    str_h = check_h(str_h);

    //  Ширина места для одной цифры
    uint8_t  w = frame_text_char_width('0');
    uint16_t y = (SCREEN_H - str_h) >> 1;
    uint16_t x = (w << 3);      //  DD.MM.YY

    //  Проверка возможности разместить виджет на экране
    if ((str_h == 16) && !((*((uint8_t*)current_font)) >> 4)) {
        if ((x << 1) > SCREEN_W) {
            frame_text_char(0, y, 'E');
            return;
        }
        x = (SCREEN_W - (x << 1)) >> 2;
    } else {
        if (x > SCREEN_W) {
            frame_text_char(0, y, 'E');
            return;
        }
        x = (SCREEN_W - x) >> 1;
    }

    //  Проверка буфера:
    if ((buf[0] <  1) || (buf[0] > 31)) buf[0] = 1;
    if ((buf[1] <  1) || (buf[1] > 12)) buf[1] = 1;
    if ((buf[2] < 20) || (buf[2] > 99)) buf[2] = 20;

    //  Изменение цифры под курсором
    switch (cursor) {
    case 0:             //  Day
        if (incr) (buf[0] < day_in_month[(uint8_t)buf[1] - 1]) ? (buf[0]++) : (buf[0] = 1);
        if (decr) (buf[0] > 1) ? (buf[0]--) : (buf[0] = day_in_month[(uint8_t)buf[1] - 1]);
        break;
    case 1:             //  Month
        if (incr) (buf[1] < 12) ? (buf[1]++) : (buf[1] =  1);
        if (decr) (buf[1] >  1) ? (buf[1]--) : (buf[1] = 12);
        break;
    case 2:             //  Year
        if (incr) (buf[2] < 99) ? (buf[2]++) : (buf[2] = 20);
        if (decr) (buf[2] > 20) ? (buf[2]--) : (buf[2] = 99);
        if (buf[0] > day_in_month[(uint8_t)buf[1] - 1]) buf[0] = day_in_month[(uint8_t)buf[1] - 1];
        if ((buf[2] & 0x03) && (buf[1] == 2) && (buf[0] == 29)) buf[0] = 28;
        break;
    }

    //  Вывод цифр и курсора
    if ((*((uint8_t*)current_font)) >> 4) {         //  Если текущий шрифт 16 пикс.
        for (uint8_t i = 0; i < 3; i++) {
            if (cursor == i) frame_draw_rect((x - 3), (y - 2), (x + (w << 1)), (y + 17), 0);
            frame_text_char(x, y, (0x30 + (buf[i] / 10)));
            x += w;
            frame_text_char(x, y, (0x30 + (buf[i] % 10)));
            x += w;
            if (i < 2) frame_text_char((x + (w >> 1) - 2), y, '.');
            x += w;
        }
    } else {                                        //  Если текущий шрифт 8 пикс.
        uint8_t l, r;
        for (uint8_t i = 0; i < 3; i++) {
            if (cursor == i) {
                if (str_h == 16) {                  //  Если будет масштабирование строки то вычисляем края курсора
                    l = (x << 1) - 3;
                    r = (x + (w << 1)) << 1;
                } else {                            //  Без масштабирования отрисовываем курсор сразу
                    frame_draw_rect((x - 2), (y - 1), (x + (w << 1)), (y + 9), 0);
                }
            }
            frame_text_char(x, y, (0x30 + (buf[i] / 10)));
            x += w;
            frame_text_char(x, y, (0x30 + (buf[i] % 10)));
            x += w;
            if (i < 2) frame_text_char((x + (w >> 1) - 1), y, '.');
            x += w;
        }
        if (str_h == 16) {                          //  Масштабирование строки и отрисовка курсора
            frame_string_scale(((SCREEN_H - y) >> 3) - 1);
            if (cursor < 3) frame_draw_rect(l, (y - 1), r, (y + 18), 0);
        }
    }
}


//  Ввод строки
void widget_build_string (char *buf, uint8_t size, uint8_t str_h, uint8_t cursor, uint8_t incr, uint8_t decr)
{
    //  Выход, если используется слишком высокий шрифт
    if (((*((uint8_t*)current_font)) >> 4) > 1) return;

    //  Проверка корректного значения высоты строки
    str_h = check_h(str_h);

    //  Переменные для отображения строки
    uint8_t x = 0, l = 0, y = (SCREEN_H - str_h) >> 1;
    cursor <<= 1;
    size <<= 1;

    //  Изменение выбранного символа
    widget_symbol_select(buf, cursor, incr, decr);

    //  Отображение строки на дисплее
    for (uint8_t i = 0; i < size; i += 2) {
        if (i == cursor) l = x;

        if (buf[i] == 0x00) {
            x = frame_text_char(x, y, buf[i + 1]);
        } else {
            x = frame_text_char(x, y, convert_UTF8_to_font(buf[i], buf[i + 1]));
        }

        if ((str_h == 16) && !((*((uint8_t*)current_font)) >> 4)) {
            if (i == cursor) frame_draw_line((l << 1), (y - 2), ((x << 1) - 3), (y - 2));
            if ((x << 1) > SCREEN_W) return;
        } else {
            if (i == cursor) frame_draw_line(l, (y - 2), (x - 2), (y - 2));
            if (x > SCREEN_W) return;
        }
    }

    //  Масштабирование строки при использовании однострочного шрифта
    if ((str_h == 16) && !((*((uint8_t*)current_font)) >> 4)) {
        frame_string_scale(((SCREEN_H - y) >> 3) - 1);
    }
}


//  Заполнение строки символами '-'
void widget_str_prepare (char *buf, uint8_t size)
{
    size <<= 1;
    for (uint8_t i = 0; i < size; i += 2) {
        buf[i]   = 0x00;
        buf[i + 1] = '-';
    }
    buf[size] = '\0';
}


//  Удаление лишних нулей из строки
void widget_str_normalize (char *buf, uint8_t buf_size)
{
    uint8_t pos = 0;

    //  Удаление "лишних" нулей
    for (uint8_t i = 0; i < (buf_size - 2); i += 2) {
        if ((buf[i] == 0x00) && (buf[i + 1] != 0x00)) {
            buf[pos++] = buf[i + 1];
        } else if ((buf[i] != 0x00) && (buf[i + 1] != 0x00)) {
            buf[pos++] = buf[i];
            buf[pos++] = buf[i + 1];
        } else {
            break;
        }
    }
    //  Заполнение нулями массива до конца
    while (pos < buf_size) buf[pos++] = '\0';

    //  Проверки формата строки:
    if ((buf[0] == ' ') || (buf[0] == '.')) buf[0] = '_';   //  строка не может начинаться с пробела или точки

    for (pos = (buf_size - 1); pos > 0; pos--) {
        if ((buf[pos] != '\0') && (buf[pos] != ' ') && (buf[pos] != '.')) {
            break;
        } else if ((buf[pos] == ' ') || (buf[pos] == '.')) {
            buf[pos] = '\0';
        }
    }
}

/*
//  Заполнение нулями массива widget_buf
void widget_buf_clear (void)
{
    for (uint8_t i = 0; i < WIDGET_BUF_SIZE; i++) widget_buf[i] = 0x00;
}
*/

/*-------------------------------------------------------- Private functions: */
//  Установка высоты строки в зависимости от выбранного шрифта
static uint8_t check_h (uint8_t str_h)
{
    if ((*((uint8_t*)current_font)) >> 4) {
        str_h = 16;
    } else if (str_h != 16) {
        str_h = 8;
    }
    return str_h;
}

//  Изменение сомвола в строке
static void widget_symbol_select (char *buf, uint8_t cursor, uint8_t incr, uint8_t decr)
{
    if (incr) {
        if (((uint8_t)buf[cursor] == 0x00) && ((uint8_t)buf[cursor + 1] == 0x20)) {
            buf[cursor]     = 0x00;
            buf[cursor + 1] = 0x2D;
        } else if (((uint8_t)buf[cursor] == 0x00) && ((uint8_t)buf[cursor + 1] == 0x2E)) {
            buf[cursor]     = 0x00;
            buf[cursor + 1] = 0x30;
        } else if (((uint8_t)buf[cursor] == 0x00) && ((uint8_t)buf[cursor + 1] == 0x39)) {
            buf[cursor]     = 0xD0;
            buf[cursor + 1] = 0x90;
        } else if (((uint8_t)buf[cursor] == 0xD0) && ((uint8_t)buf[cursor + 1] == 0xAF)) {
            buf[cursor]     = 0x00;
            buf[cursor + 1] = 0x20;
        } else {
            buf[cursor + 1]++;
        }
    } else if (decr) {
        if (((uint8_t)buf[cursor] == 0x00) && ((uint8_t)buf[cursor + 1] == 0x20)) {
            buf[cursor]     = 0xD0;
            buf[cursor + 1] = 0xAF;
        } else if (((uint8_t)buf[cursor] == 0x00) && ((uint8_t)buf[cursor + 1] == 0x2D)) {
            buf[cursor]     = 0x00;
            buf[cursor + 1] = 0x20;
        } else if (((uint8_t)buf[cursor] == 0x00) && ((uint8_t)buf[cursor + 1] == 0x30)) {
            buf[cursor]     = 0x00;
            buf[cursor + 1] = 0x2E;
        }else if (((uint8_t)buf[cursor] == 0xD0) && ((uint8_t)buf[cursor + 1] == 0x90)) {
            buf[cursor]     = 0x00;
            buf[cursor + 1] = 0x39;
        } else {
            buf[cursor + 1]--;
        }
    }
}
