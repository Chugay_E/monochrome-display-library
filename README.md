# Monochrome display library.

**For STM32 MCU. Use CMSIS 4.2.0.**\
[CMake](https://cmake.org/)\
[GCC arm-none-eabi-*** toolchain](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads).

---

## Display controller: 
- SH1106;
- SSD1306;
- PCD8544.

---

## Description: 
- Initialisation used MCU interface;
- Initialisation display controller;
- Frame-build function;
- Draw grafical primitives;
- Show text information;
- Base widgets for make scroll-menu, insert number or symbol string.
